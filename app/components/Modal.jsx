import React from 'react';

class Modal extends React.Component {

  getStudentsData() {
    const {students} = this.props;

    let presents = [],
        lates = [],
        absents = [],
        unmarked = [];
    // Sort students by status
    for (let i in students) {
      if (students.hasOwnProperty(i)) {
        const name = students[i].firstName+' '+students[i].lastName,
              id = students[i].id,
              mark = students[i].attendanceMark;

        if (mark.present) {
          presents.push({id: id, name: name, status: 'Present'});
        } else if (mark.late){
          lates.push({id: id, name: name, status: 'Late'});
        } else if (mark.absent){
          absents.push({id: id, name: name, status: 'Absent'});
        } else {
          unmarked.push({id: id, name: name, status: 'Unmarked'});
        }
      }
    }
    // Concat to one array with a order
    return [...presents, ...lates, ...absents, ...unmarked];
  }

  render() {
    const data = this.getStudentsData();
    return (
      <div className="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="modal-title" id="exampleModalLabel">The list of students</h2>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <ul className="list-group">
                {data.map((student, index) =>
                  <li className="list-group-item" key={student.id}>{index+1}. {student.name} - {student.status}</li>
                )}
              </ul>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Modal;