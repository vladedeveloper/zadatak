import React from 'react';

// Use stateless function
export const MultiMark = (props) => {
    return (
      <div className="col-md-3">
        <div>
          <button className="btn btn-success" onClick={props.selectedPresent}>Present</button>
          <button className="btn" onClick={props.selectedLate}>Late</button>
          <button className="btn btn-danger" onClick={props.selectedAbsent}>Absent</button>
        </div>
      </div>
    )
}

export default MultiMark;