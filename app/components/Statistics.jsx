import React from 'react';

class Statistics extends React.Component {

  getStatisticsData() {
    const {students} = this.props;
    let data = {
      present: 0,
      late: 0,
      absent: 0,
      unmarked: 0
    };
    // Classify statuses
    for (let i in students) {
      if (students.hasOwnProperty(i)) {
        if (students[i].attendanceMark.present) {
          data.present++;
        } else if (students[i].attendanceMark.late) {
          data.late++;
        } else if (students[i].attendanceMark.absent) {
          data.absent++;
        } else {
          data.unmarked++;
        }
      }
    }
    return data;
  }

  render() {

    const data = this.getStatisticsData();

    return (
      <div className="col-md-3 pull-right">
        <p>Present: {data.present}</p>
        <p>Late: {data.late}</p>
        <p>Absent: {data.absent}</p>
        <p>Unmarked: {data.unmarked}</p>
        <div>
          <button className="btn btn-primary" onClick={this.props.resetStatus}>Reset</button>
          <button className="btn btn-success" onClick={this.props.showModal}>Done</button>
        </div>
      </div>
    )
  }
}

export default Statistics;