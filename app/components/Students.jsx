import React from 'react';

class Students extends React.Component {

  getClassNames() {
    const {attendanceMark} = this.props;
    let classes = ['thumbnail'];
    // Add appropriate class
    if (attendanceMark.present) {
      classes.push("present");
    } else if (attendanceMark.late) {
      classes.push("late");
    } else if (attendanceMark.absent) {
      classes.push("absent");
    }

    return classes.join(' ');
  }

  // Use propriate html according to status
  renderMark() {
    const {attendanceMark} = this.props;

    if (attendanceMark.present) {
      return (
        <div className="marked"><span className="glyphicon glyphicon-ok"></span><strong>Present</strong></div>
      );
    } else if (attendanceMark.late) {
      return (
        <div className="marked"><span className="glyphicon glyphicon-time"></span><strong>Late</strong></div>
      );
    } else if (attendanceMark.absent) {
      return (
        <div className="marked"><span className="glyphicon glyphicon-remove"></span><strong>Absent</strong></div>
      );
    } else {
      return (
        <div className="mark">
          {/* Use partial function to not change this */}
          <span className="glyphicon glyphicon-ok" onClick={this.props.updateStatus.bind(null, "present", this.props.id)}></span>
          <span className="glyphicon glyphicon-time" onClick={this.props.updateStatus.bind(null, "late", this.props.id)}></span>
          <span className="glyphicon glyphicon-remove" onClick={this.props.updateStatus.bind(null, "absent", this.props.id)}></span>
        </div>
      );
    }
  }

  render (){
    return(
      <div className="col-md-3">
        <div id={this.props.id} className={this.getClassNames()}>
          <div className="caption" onClick={this.props.selectStudent.bind(null, this.props.id)}>
            <img src={this.props.image} className="img-thumbnail" height="100" width="100" />
            <h6>{this.props.firstName} {this.props.lastName}</h6>
          </div>
          {this.renderMark()}
        </div>
      </div>
    )
  }
}

export default Students;