import React from 'react';
import Students from '../components/Students';
import MultiMark from '../components/MultiMark';
import Statistics from '../components/Statistics';
import Modal from '../components/Modal';
import students from '../../public/data/students';

class Main extends React.Component {

  constructor() {
    super();
    // Make an array of default status values
    this.defaultStatus = [];
    for (let i in students) {
      if (students.hasOwnProperty(i)) {
        this.defaultStatus[students[i].id] = {
          present: students[i].attendanceMark.present,
          absent: students[i].attendanceMark.absent,
          late: students[i].attendanceMark.late,
        };
      }
    }
  }

  componentWillMount() {
    this.setState({
      students: students
    });
  }
  /* Use arrow function to avoid bind */ 
  updateStatus = (status, id) =>  {
    const {students} = this.state;
    // Update status of desired student
    students.map((item, index) => {
      if (item.id === id) item.attendanceMark[status] = true;
      return item;
    });

    this.setState({students:students}); 
  }

  resetStatus = () => {
    // Change status of students to default values
    students.map((item) => {
      item.attendanceMark.present = this.defaultStatus[item.id].present;
      item.attendanceMark.absent = this.defaultStatus[item.id].absent;
      item.attendanceMark.late = this.defaultStatus[item.id].late;
      return item;
    });
    // Remove selected students
    $(".selected").removeClass("selected");
    this.setState({students:students}); 
  }

  showModal = () =>  {
    $("#Modal").modal("show");
  }

  selectStudent = (id) => {
    $("#"+id).toggleClass("selected");
  }

  selectedPresent = () => {
    this.selectedChange("present");
  }

  selectedLate = () => {
    this.selectedChange("late");
  }

  selectedAbsent = () => {
    this.selectedChange("absent");
  }

  selectedChange(type) {
    // Get all selected students
    const selected = $(".selected").removeClass("selected"),
          {students} = this.state;
    let ids = [];
    // Get students ids
    selected.each(function(index) {
      ids.push(Number($(this).attr("id")));
    });
    // Change status of selected students
    students.map((item, index) => {
      if (ids.includes(item.id)) {
        item.attendanceMark.present = false;
        item.attendanceMark.late = false;
        item.attendanceMark.absent = false;
        item.attendanceMark[type] = true;
      }
      return item;
    });

    this.setState({students:students}); 
  }

  render() {
    return (
      <div className="container">
        <section>
          <div className="row">
            {this.state.students.map(info =>
              <Students key={info.id} 
                            {...info} 
                            updateStatus = {this.updateStatus}
                            selectStudent = {this.selectStudent}
              />
            )}
          </div>
        </section>
        <footer>
          <div className="row">
            <MultiMark selectedPresent = {this.selectedPresent}
                       selectedLate = {this.selectedLate}
                       selectedAbsent = {this.selectedAbsent}
            />
            <Statistics resetStatus = {this.resetStatus}
                        students = {this.state.students}
                        showModal = {this.showModal}
            />
          </div>
        </footer>
        <Modal students = {this.state.students} />
      </div>
    )
  }
}

export default Main;