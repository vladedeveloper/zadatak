import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { browserHistory } from 'react-router';

import "../public/css/style.scss"; 

import Main from './pages/Main';

ReactDOM.render(
   <BrowserRouter history={browserHistory}>
    <Route path="/" component={Main}>
    </Route>
  </BrowserRouter>,
  document.getElementById('wrapper')
);